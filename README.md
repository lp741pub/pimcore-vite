# Pimcore Vitejs add-on

Pimcore Vitejs is a set of helpers and configuration to get vite hrm and build direct into your Pimcore project

## Requirements

- [Pimcore X](https://pimcore.com/docs/pimcore/current/Development_Documentation/index.html) 
- [lp741/node-pnpm](https://gitlab.com/lp741pub/node-pnpm) Docker image


Follow instruction at [lp741/node-pnpm](https://gitlab.com/lp741pub/node-pnpm) how to create a Docker Image

## Installation

Copy all files in this repository directly inside your Pimcore X root directory **except for config folder**

Paste config/service.yaml in your <pimcore project>/config/service.yaml

```yaml
# <pimcore project>/config/service.yaml
services:
  # ......
  
  # ---------------------------------------------------------
  # Twig Extensions
  # ---------------------------------------------------------
  App\Twig\Extension\ViteExtension:
    tags: ['twig.extension']
```

## Usage

Inside your layout twig template put helper `{{ app_vite('main.js') | raw }}` before head closing tag or wherever you prefer

Ex. &lt;pimcore-project-root&gt;/templates/layouts/layout.html.twig

```html
<!DOCTYPE html>
<html lang="{{ app.request.locale }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ pimcore_head_title() }}
    {{ pimcore_head_meta() }}
    {{ pimcore_head_link() }}
    ....
    {{ app_vite('main.js') | raw }} {# vite helper #}
</head>
```

run vite in debug mode

```bash
docker-compose -f docker-compose-vite.yaml up
```

now every change in vite folder or in twig templates causes UI updated without need to reload page

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)