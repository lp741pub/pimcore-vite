<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace App\Twig\Extension;

// Helpers here serve as example. Change to suit your needs.

// For a real-world example check here:
// https://github.com/wp-bond/bond/blob/master/src/Tooling/Vite.php
// https://github.com/wp-bond/boilerplate/tree/master/app/themes/boilerplate

// on the links above there is also example for @vitejs/plugin-legacy



// Prints all the html entries needed for Vite

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ViteExtension extends AbstractExtension
{

  public function vite(string $entry): string
  {
    return "\n" . $this->jsTag($entry);
     // . "\n" . $this->jsPreloadImports($entry)
     // . "\n" . $this->cssTag($entry);
  }


// Some dev/prod mechanism would exist in your project

  function isDev(string $entry): bool
  {
    // This method is very useful for the local server
    // if we try to access it, and by any means, didn't started Vite yet
    // it will fallback to load the production files from manifest
    // so you still navigate your site as you intended!

    static $exists = null;
    if ($exists !== null) {
      return $exists;
    }
    $handle = curl_init('http://sogemi-www-vite-1:3000/' . $entry);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_NOBODY, true);

    curl_exec($handle);
    $error = curl_errno($handle);
    curl_close($handle);

    return $exists = !$error;
  }


// Helpers to print tags

  function jsTag(string $entry): string
  {
    $url = $this->isDev($entry)
      ? 'http://localhost:3000/' . $entry
      : $this->assetUrl($entry);

    if (!$url) {
      return '';
    }
    return '<script type="module" crossorigin src="'
      . $url
      . '"></script>';
  }

  function jsPreloadImports(string $entry): string
  {
    if ($this->isDev($entry)) {
      return '';
    }

    $res = '';
    foreach ($this->importsUrls($entry) as $url) {
      $res .= '<link rel="modulepreload" href="'
        . $url
        . '">';
    }
    return $res;
  }

  function cssTag(string $entry): string
  {
    // not needed on dev, it's inject by Vite
    if ($this->isDev($entry)) {
      return '';
    }

    $tags = '';
    foreach ($this->cssUrls($entry) as $url) {
      $tags .= '<link rel="stylesheet" href="'
        . $url
        . '">';
    }
    return $tags;
  }


// Helpers to locate files

  function getManifest(): array
  {
    $content = file_get_contents(__DIR__ . '/dist/manifest.json');

    return json_decode($content, true);
  }

  function assetUrl(string $entry): string
  {
    $manifest = $this->getManifest();

    return isset($manifest[$entry])
      ? '/dist/' . $manifest[$entry]['file']
      : '';
  }

  function importsUrls(string $entry): array
  {
    $urls = [];
    $manifest = $this->getManifest();

    if (!empty($manifest[$entry]['imports'])) {
      foreach ($manifest[$entry]['imports'] as $imports) {
        $urls[] = '/dist/' . $manifest[$imports]['file'];
      }
    }
    return $urls;
  }

  function cssUrls(string $entry): array
  {
    $urls = [];
    $manifest = $this->getManifest();

    if (!empty($manifest[$entry]['css'])) {
      foreach ($manifest[$entry]['css'] as $file) {
        $urls[] = '/dist/' . $file;
      }
    }
    return $urls;
  }


  /**
   * @inheritDoc
   */
  public function getFunctions(): array
  {
    return [
      new TwigFunction('app_vite', [$this, 'vite'])
    ];
  }
}