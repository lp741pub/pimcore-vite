/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './vite/src/**/*.{vue,js,ts,jsx,tsx}',
    './templates/**/*.twig'
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
